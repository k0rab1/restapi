#Test RESTful API app for "RuYOU"

Clone this repo to your local server (or hosting).

Create DataBase and connecting her to your app
```
common/config/main-local.php
console/config/main-local.php
```

Do the migration:
```
$ yii migrate
```

For work with REST, go to: 
```
http://your-domain.ru/frontend/web/users
```

To authentication in api use Baerer Token
```
1a1Sc6sGJmDGGUHfKHw_x-jB2bCx-K_E_1594582877
```
