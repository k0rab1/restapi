<?php
/**
 * Created by PhpStorm.
 * User: korabl
 * Date: 12.07.2020
 * Time: 20:36
 */

namespace frontend\controllers;

use frontend\resource\User;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;

class UserController extends ActiveController
{
    public $modelClass = User::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
            ],
        ];
        return $behaviors;
    }

}