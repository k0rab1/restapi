<?php
/**
 * Created by PhpStorm.
 * User: korabl
 * Date: 12.07.2020
 * Time: 20:39
 */

namespace frontend\controllers;

use frontend\resource\Profile;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class ProfileController extends ActiveController
{
    public $modelClass = Profile::class;

    public function actions()
    {
        $action = parent::actions();

        $action['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
    }

    public function prepareDataProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->modelClass::find()->andWhere(['user_id' => \Yii::$app->request->get('userid')])
        ]);
    }
}