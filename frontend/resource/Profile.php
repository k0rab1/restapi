<?php
/**
 * Created by PhpStorm.
 * User: korabl
 * Date: 12.07.2020
 * Time: 23:04
 */

namespace frontend\resource;

class Profile extends \common\models\Profile
{
    public function fields()
    {
        return ['name', 'last_name', 'phone'];
    }

}