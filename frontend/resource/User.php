<?php
/**
 * Created by PhpStorm.
 * User: korabl
 * Date: 12.07.2020
 * Time: 21:46
 */

namespace frontend\resource;

class User extends \common\models\User
{
    public function fields()
    {
        return ['id', 'username', 'email', 'profiles'];
    }

    public function extraFields()
    {
        return ['verification_token'];
    }
    public function getProfiles(){
        return $this->hasMany(Profile::class, ['user_id' => 'id']);
    }
}